# Bundlicious

This is the P.O.C ( 3rd rev ) for Infoblox's Support Bundle, traffic capture and database analytics + automation. 

Full comprehensive concept for bundlicious was to be the "technical" platform for Infoblox's NIOS troubleshoot + review etc. This's include but not limited to

* Automation to process the payloads ( support bundle, database, pcap, etc )
* Analytics of the uploaded payloads
* Notification
* integration with Lab
* integration with CRM


## Current POC features

* High level overview of the support bundle.
* Interactive and synchronised charts for
	* CPU/Memory utilisation from ptop data
	* smartnic details info for QPS, QPS to named,
	* named Cache Hit Rates by views


-* additional features could be added depending on author availability *-

## Getting Started

Download the jar file from [Download](https://bitbucket.org/suriaville/bundlicious/downloads/) section. Execute as follow.


```
java -jar bundlicious-x.x-SNAPSHOT.jar -d sb
```

where sb is the directory path for extracted Infoblox's support bundle.

### Prerequisites

Java 8.

### Installing

Make sure Java 8 is installed and properly configured.

## Deployment

Starting from SNAPSHOT 1.1, bundlicious is supported in Windows + *nix ( including MacOS )

## Built With

* [Kotlin](https://kotlinlang.org)
* [Gradle](https://gradle.org) - build automation tool focused on flexibility and performance.
* [Unix4j](https://github.com/tools4j/unix4j) - implementation of Unix command line tools in Java
* [Apache Commons CLI](https://commons.apache.org/proper/commons-cli/) -  library which provides an API for parsing command line options
* [Javalin](https://javalin.io) - A simple web framework for Java and Kotlin
* [Apache FreeMarker](https://freemarker.apache.org) - template engine
* [koda-time](https://github.com/debop/koda-time) - Joda Time Extensions in Kotlin
* [Apache Tika](https://tika.apache.org) - toolkit detects and extracts metadata and text from over a thousand different file types
* [Apache Commons Collections](http://commons.apache.org/proper/commons-collections/) - provides powerful data structures that accelerate development 


## Authors

**alex c** - *Initial work* - [Suriaville](https://bitbucket.org/suriaville/bundlicious/)

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE version 3 by Free Software Foundation, Inc.  - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Infoblox's APAC Technical Support Team
