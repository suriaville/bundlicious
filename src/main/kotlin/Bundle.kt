import org.apache.commons.collections4.MultiValuedMap
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap
import org.apache.commons.collections4.multimap.HashSetValuedHashMap
import java.io.File
import java.util.zip.GZIPInputStream
import org.apache.tika.Tika
import org.unix4j.Unix4j
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.ArrayList


class Bundle ( val sbDirectory: String? ) {

    var sn: String? = null
    var hwtype: String? = null
    var hwid: String? = null
    var niosVersion: String? = null
    var cluster: String? = null
    var licenses: String? = null
    var vrid: String? = null
    var ipAddresses: String? = null
    var ipConfig: String? = null
    var ifconfig: String? = null
    var upgrade: String? = null
    var hostname: String? = null
    var smart: String? = null
    var firmware: String? = null
    var timezone: String? = null
    var files = mutableListOf<fileAtom>()
    var chrRaw: MultiValuedMap<String, List<String>> = ArrayListValuedHashMap()
    var chrRaw2: MultiValuedMap<String, List<String>> = HashSetValuedHashMap()
    var chrRaw3: HashMap<String, MutableList<chrAtom>> = hashMapOf()
    var chrView = mutableSetOf<String>()

    var chrString = mutableMapOf<String,String>()
    var chrNum :Boolean = false
    var supportTransfer: List<String> = listOf<String>()
    var niosServices :HashMap<String, String> = hashMapOf()
    var netLAN1 :List<String> = listOf()
    var netLAN2 :List<String> = listOf()
    var netMGMT :List<String> = listOf()
    var netHA :List<String> = listOf()
    var netVIP :List<String> = listOf()
    var netTun :List<String> = listOf()
    var sbTimeZone: String? = null
    var ip_addresses: String? = null

    var sbChartSignal :HashMap<String, Boolean> = hashMapOf("snicQPS" to false, "cancel" to false)
    var pQPSChart = false
    var pTopCPUtemp: MultiValuedMap<String, List<String>> = ArrayListValuedHashMap()
    var pTopCPU :MutableList<String> = mutableListOf()
    var pTopCPUString : String? = null
    var pTopMemString : String? = null
    var pTopCPUMemString : String? = null
    var pTopMem: MultiValuedMap<String, List<String>> = ArrayListValuedHashMap()
    var pQPSstring :String? = "DateTime, oct0TotalQPS, oct0SnicQPS, oct0QPStoBIND, oct1TotalQPS, oct1SnicQPS, oct1QPStoBIND, oct2TotalQPS, oct2SnicQPS, oct2QPStoBIND, oct3TotalQPS, oct3SnicQPS, oct3QPPStoBIND, ActiveCacheInSnic, PrefetchToBIND, FreeInSnic\n"
    var pQPSstring1 :String? = "DateTime, oct0 Total QPS, oct0 QPS, oct0 to BIND, oct1 Total QPS, oct1 QPS, oct1 to BIND, oct2 Total QPS, oct2 QPS, oct2 to BIND, oct3 Total QPS, oct3 QPS, oct3 to BIND, ActiveCacheEntriesInSmartnic, PrefetchQueriestoBIND, FreeEntriesInCard\n"
    var numOfCPU: Int? = null
    var tempNumOfCPU  = false


    init {

        val listOfFile = File(sbDirectory).walk().sorted().toList()
        val log_infoblox = File ( sbDirectory + "/infoblox.log" )
        val log_sn = File ( sbDirectory + "/support_serial_number.txt" )
        val log_messages = File ( sbDirectory + "/var/log/messages" )

        if (  log_infoblox in listOfFile && log_sn in listOfFile ) {
            println("looks like a support bundle...\nProceed...")
            if ( log_infoblox.exists() ) fun_infoblox(log_infoblox)

            for ( num in 9 downTo 0) { // there is a reason for this for, instead of looping in the listOfFile.foreach below... :-) //

            val syslogGzFile = File(sbDirectory + "/var/log/messages." + num.toString() + ".gz")
            val syslogFile = File(sbDirectory + "/var/log/messages." + num.toString())

            if ( syslogGzFile.exists() ) {

                if ( Tika().detect(syslogGzFile).contains("gzip") ) {

                    println("extracting ${syslogGzFile}!")
                    GZIPInputStream(syslogGzFile.inputStream()).copyTo(syslogFile.outputStream())
                    syslogGzFile.delete()
                    fun_syslog(syslogFile)

                } else {

                    println("Skipping ${syslogGzFile} due to unknown format!")

                }

            } else if ( syslogFile.exists() && Tika().detect(syslogFile).contains("text/plain") ) {

                fun_syslog(syslogFile)

            }
        }

            if ( log_messages.exists() ) fun_syslog(log_messages)


            listOfFile.forEach {
                when {
                    Regex("cluster_status.txt$").containsMatchIn(it.toString())             -> this.cluster         = it.readText().trim()
                    Regex("hwtype.txt$").containsMatchIn(it.toString())                     -> this.hwtype          = it.readText().trim()
                    Regex("smart.log").containsMatchIn(it.toString())                       -> this.smart           = it.readText().trim()
                    Regex("show_firmware.txt").containsMatchIn(it.toString())               -> this.firmware        = it.readText().trim()
                    Regex("vrid.txt$").containsMatchIn(it.toString())                       -> this.vrid            = it.readText().trim()
                    Regex("ip_addresses.txt$").containsMatchIn(it.toString())               -> this.ip_addresses     = it.readText().trim()
                    Regex("upgrade.log$").containsMatchIn(it.toString())                    -> this.upgrade         = it.readText().trim()
                    Regex("support_licenses.txt$").containsMatchIn(it.toString())           -> this.licenses        = it.readText().trim()
                    Regex("support_serial_number.txt$").containsMatchIn(it.toString())      -> this.sn              = ":\\s+(\\S+)".toRegex().find(it.readText())!!.destructured.component1()
                    Regex("support_hwid.txt$").containsMatchIn(it.toString())               -> this.hwid            = ":\\s+(\\S+)".toRegex().find(it.readText())!!.destructured.component1()
                    Regex("ptop-").containsMatchIn(it.toString())                           -> fun_ptop(it)
                    Regex("sysctl.txt").containsMatchIn(it.toString()) && !Regex("alternate_partition_sb").containsMatchIn(it.toString())   -> fun_sysctl(it)
                    Regex("ifconfig.txt$").containsMatchIn(it.toString())                   -> fun_networkIF(it)
                    Regex("/firewall/status/").containsMatchIn(it.toString())               -> fun_services(it)
                    Regex("ifconfig.txt$").containsMatchIn(it.toString())                   -> fun_networkIF(it)
                    Regex("/firewall/status/").containsMatchIn(it.toString())               -> fun_services(it)
                }
            }

            var tempA1 = mutableListOf<String>("datetime ")
            pTopCPUString = "datetime, cpu "

            for ( val01 in 1 .. this.numOfCPU!!.toInt() ) {
                tempA1.add("cpu${val01 -1 }" )
                pTopCPUString = pTopCPUString + ", cpu${val01 - 1 }"
                pTopCPUMemString = pTopCPUString + ", cpu${val01 - 1}"
            }

            pTopCPUMemString = pTopCPUString + ", mem\n"
            pTopCPUString = pTopCPUString + "\n"

            var tempCPUarray = mutableListOf<String>()
            var tempMEMarray = mutableListOf<String>()
            var tempCPUMEMString : String? = null

            pTopCPU.add(tempA1.toList().toString().replace("[","").replace("]",""))

            for ( kunci in pTopCPUtemp.keys().sorted().distinct()) {

                val testValue = pTopCPUtemp.get(kunci)

                testValue.forEach {

                    pTopCPU.add(it.toString().replace("[","").replace("]",""))
                    //pTopCPUString = pTopCPUString + it.replace("[","").replace("]","").toString()
                    pTopCPUString = pTopCPUString + it.toString().replace("[","").replace("]","") + "\n"
                    tempCPUMEMString = pTopCPUString + it.toString().replace("[","").replace("]","")

                    tempCPUarray.add(it.toString().replace("[","").replace("]","") )
                }
            }

            pTopMemString = "datetime, Memory Used \n"
            for ( kunci in pTopMem.keys().sorted().distinct()) {

                val testValue = pTopMem.get(kunci)

                testValue.forEach {

                    pTopMemString = pTopMemString + it[0].toString().replace("[","").replace("]","").removeRange(16,19) + " , " + "100.0".toFloat().minus(it[2].toFloat()) + "\n"

                    tempMEMarray.add( "100.0".toFloat().minus(it[2].toFloat()).toString() )

                }
            }


            for ( y in 0 .. tempCPUarray.size - 1 ) {
                pTopCPUMemString = pTopCPUMemString + tempCPUarray[y] + ", " + tempMEMarray[y] + "\n"
            }

            if ( !chrView.isEmpty() ) {


                for ( ( kk , vv ) in chrRaw3) {

                    val viewName = kk.toString()

                    vv.sortBy { it.epoch }
                    vv.forEach {

                        if ( vv.indexOf(it) != 0 ) {

                            var v = vv[vv.indexOf(it) - 1]
                            var delta = it.epoch - v.epoch
                            var MemSize = it.size - v.size
                            var qps = it.total / delta
                            var n_hits = it.hits - v.hits
                            var n_misses = it.misses - v.misses
                            var n_total = n_hits + n_misses
                            var n_qps = n_total/delta

                            var t4 = "${it.datetime.replace("T"," ")} , ${it.size/1048576} , ${n_hits} , ${n_misses} , ${n_total} , ${n_qps}\n"

                            if ( !this.chrString.containsKey(viewName) ) {

                                this.chrString.put(viewName, t4 )

                            } else {

                                this.chrString.put(viewName, "${this.chrString[viewName]}${t4}")

                            }

                            this.chrNum = true

                        } else {

                        }
                    }

                }
            }

        } else {
            println("doesn't looks like a support bundle...\nkthanxbai...")
        }


    }

    fun fun_infoblox ( inFile: File ) {

        println("processing infoblox.log")

        inFile.forEachLine {

            var sbTransferRegex = """\[([\d+\/\s:]+)\.\d{3}.+getting support file: supportBundle:(.+)""".toRegex()

            var matchSBTransfer = sbTransferRegex.find(it)

            if ( matchSBTransfer != null ) {
                this.supportTransfer = matchSBTransfer.destructured.toList()
            }
        }

        var headCounter = 0
        val largeOne = File(inFile.absoluteFile.toString()).readLines()
        var first :String = ""
        var second :String = ""
        var ( pertama, kedua) = largeOne[headCounter].split(" ")

        while ( pertama == "" ) { // to ensure timestamp is really assigned at the beginning
            headCounter++
            var ( pertama1, kedua2) = largeOne[headCounter].split(" ")
            pertama = pertama1
            kedua = kedua2
        }
        first = pertama
        second = kedua


        var omaha = largeOne.lastIndex
        val omaha1 = largeOne.last()
        val omaha2 = largeOne.count()
        val omaha3 = Unix4j.tail(1,inFile).toString()

        var last :String = ""
        var secondLast :String = ""
        var ( akhir, secondakhir) = largeOne[omaha].split(" ")

        while ( akhir == "" ) {
            omaha--
            var ( akhir1, secondakhir2) = largeOne[omaha].split(" ")
            akhir = akhir1
            secondakhir = secondakhir2
        }
        last = akhir
        secondLast = secondakhir



        var wet = mutableMapOf<String, Int>()

        first = first.replace("[", " ").replace("/", "-")
        second = second.replace("]", " ")
        last = last.replace("[", " ").replace("/", "-")
        secondLast = secondLast.replace("]", " ").replace("/", "-")

        val got1 = (first + "T" + second + "+07:00").replace("\\.\\d\\d\\d".toRegex(), "").replace(" ","")
        val got2 = (last + "T" + secondLast + "+07:00").replace("\\.\\d\\d\\d".toRegex(), "").replace(" ","")

        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24

        var firstEpoch = OffsetDateTime.parse(got1, DateTimeFormatter.ISO_DATE_TIME)
        var lastEpoch = OffsetDateTime.parse(got2, DateTimeFormatter.ISO_DATE_TIME)

        var different = lastEpoch.toEpochSecond().minus(firstEpoch.toEpochSecond()).times(1000)

        val elapsedDays = different / daysInMilli
        different = different % daysInMilli

        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli

        val elapsedMinutes = different / minutesInMilli
        different = different % minutesInMilli

        val elapsedSeconds = different / secondsInMilli

        val elapsed = ("${elapsedDays} days, ${elapsedHours} hours,<br> ${elapsedMinutes} minutes, ${elapsedSeconds} seconds")


        for ( line in largeOne ) {

            val wat = "^\\[(\\d\\d\\d\\d/\\d\\d/\\d\\d)\\s(\\d\\d:\\d\\d):\\d\\d".toRegex()
            val ( tarikh ) = line.split("]")
            val wut = wat.find(tarikh)

            if ( wut != null ) {

                var datum = wut.destructured.component1().replace("/", "-") + " " + wut.destructured.component2()
                if ( wet.containsKey(datum)) {

                    var ttmmpp = wet[datum]
                    wet.put(datum, ttmmpp!!.plus(1))

                } else {

                    wet.put(datum, 1)

                }
            }
        }

        var sparkLineString:String? = null

        val duh = fileAtom(inFile.name, first + "<br>" + second , last + "<br>" + secondLast, largeOne.size.toLong(), wet, "null" , elapsed)
        files.add(duh)

    }

    fun fun_sysctl( inFile: File ) {

        println("processing sysctl")

        val temp_sysctl = inFile.readText().trim()

        val hostname_re =  "kernel\\.hostname = ([\\d\\w\\.-]+)".toRegex()
        this.hostname = hostname_re.find(temp_sysctl)!!.destructured.component1()

    }

    fun fun_ptop ( inFile: File) {

        println("processing ${inFile.name}")

        if ( tempNumOfCPU.not() ) {
            val cpuCount = Unix4j.grep("cpu", inFile).cut("-c", "5-9").sort().uniq().toLineList()

            this.numOfCPU = cpuCount.size - 1
            this.tempNumOfCPU = true
        }

        var cpuTempArray :ArrayList<String> = arrayListOf()

        var nVersionRegex = """^IDENT\s+([\S\.\-]+)\s+""".toRegex()
        var pBlankLineRegex = """^\s*$""".toRegex()

        //  MEM t 25109409792 f 2.8 b 0.4 c 35.4 s 1.3 a 13.7 sh 45.6 sw 3.8 25769799680 pio 573.3 7332.4 sio 0.1 4.8
        var pMemRegex = """^(.+)\sMEM\s+t\s+(?<total>\S+)\s+f\s+(?<memf>\S+)\s+b\s+(?<memb>\S+)\s+c\s+(?<memc>\S+)\s+s\s+(?<mems>\S+)\s+a\s+(?<mema>\S+)\s+sh\s+(?<memsh>\S+)\s+sw\s+(?<memsw1>\S+)\s+(?<memsw2>\S+)\s+pio\s+(?<mempio1>\S+)\s+(?<mempio2>\S+)\s+sio\s+(?<memsio1>\S+)\s+(?<memsio2>\S+)""".toRegex()
        var pMemRegex1 = """MEM\s+t\s+(\S+)\s+f\s+(\S+)""".toRegex()

        //  CPU cpu0 u 21.1 id/io 78.4  0.5 u/s/n  0.1  1.2 19.7 irq h/s  0.0  0.1
        var pCPURegex = """^(.+)\sCPU\s+(\S+)\s+u\s+(\S+)\s+id/io\s+(\S+)\s+(\S+)\s+u/s/n\s+(\S+)\s+(\S+)\s+(\S+)\s+irq h/s\s+(\S+)\s+(\S+)""".toRegex()

        //  TIME 13656300.4 1519204855 2018-02-21 09:20:55
        var pTimeRegex = """^TIME\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)""".toRegex()
        var  pTime :String?   =  "0000000000"
        var  pEpoch :String?  =  "0000000000"

        // SNIC qps 0 0 159312 9695 0 0 0 0 mcore 9 0 42 41 37 5 4 4 6 19 tcore 23 49 27 21 10 16 9 9 47 21 19 10 9 16 9 9 36 16 10 16 9 9 9 8 pool 98879 8556930 147836 1578693 2146983 1991701 4091 17408 cache 420372 4349 575279 242449110
        var pQPSRegex = "^SNIC qps".toRegex()

        //var ptopFile = inFile.readText()
        inFile.forEachLine {

            var matchTime = pTimeRegex.find(it)
            var matchCPU = pCPURegex.find(pTime +" "+ it)
            var matchMem = pMemRegex.find(pTime +" "+ it)
            var matchQPS = pQPSRegex.find(pTime +" "+ it)
            var matchBlank = pBlankLineRegex.find(it)
            var matchNIOSversion = nVersionRegex.find(it)


            if ( matchNIOSversion != null ) {
                this.niosVersion = matchNIOSversion!!.destructured.component1()
            }

            if (matchTime != null) {

                pTime = matchTime!!.destructured.component3() + " " + matchTime!!.destructured.component4()
                pEpoch = matchTime!!.destructured.component2()

            } else if (matchCPU != null) {

                val ( datetime , cpu, user, id, io, dunno_u,dunno_s, dunno_n, irq_h,irq_s) = matchCPU!!.destructured

                if ( cpuTempArray.size == this.numOfCPU!! + 1 ) {

                    cpuTempArray.add(user)
                    pTopCPUtemp.put( pEpoch, cpuTempArray.toList() )
                    cpuTempArray.clear()

                } else if ( cpuTempArray.size == 0 ) {
                    cpuTempArray.add(datetime.removeRange(16,19))
                    cpuTempArray.add(user)
                } else {

                    cpuTempArray.add(user)
                }

            } else if (matchMem != null) {
                var  mem_used :Float? = matchMem.groups["memf"]!!.value.toFloat()
                mem_used = 100 - mem_used!!
                var mtemp = matchMem.groupValues
                pTopMem.put(pEpoch, matchMem.groupValues.drop(1))

            }

            if ( it.contains("^SNIC qps".toRegex())) {
                val qpsTemp = it.split("""\s+""".toRegex())
                var needtocleanupthis = pTime.toString().removeRange(16,19)

                val tR = "qps (.+) mcore (.+) tcore (.+) pool (.+) cache (.+) ".toRegex()

                val tR2 = tR.find(it)

                var tCpu = tR2!!.destructured.component1().replace(" ", ",")
                var tMcore = tR2!!.destructured.component2().replace(" ", ",")
                var tTcore = tR2!!.destructured.component3().replace(" ", ",")
                var tPool = tR2!!.destructured.component4().replace(" ", ",")
                var tCache = tR2!!.destructured.component5().split(" ").toList()

                pQPSstring = pQPSstring + "${pTime.toString().removeRange(16,19)}, ${qpsTemp[2]}, " + qpsTemp[2].toInt().minus(qpsTemp[3].toInt()) + ", ${qpsTemp[3]}, ${qpsTemp[4]}, " + qpsTemp[4].toInt().minus(qpsTemp[5].toInt()) + ", ${qpsTemp[5]}, ${qpsTemp[6]}, " + qpsTemp[6].toInt().minus(qpsTemp[7].toInt()) +  ", ${qpsTemp[7]}, ${qpsTemp[8]}, " + qpsTemp[8].toInt().minus(qpsTemp[9].toInt()) + ", ${qpsTemp[9]}," + " ${tCache[0]}, ${tCache[2]} , ${tCache[3]} \n"
                //println(pQPSstring)
                pQPSChart = true
                sbChartSignal.put("snicQPS", true)
            }

        }   // end of inFile.forEachLine

    }

    private fun fun_syslog ( inFilea: File) {

        var inFile = inFilea

        println("processing ${inFile.name}")

        if ( inFile.name.endsWith("gz")) {
            //println("need to gunzip ${inFile}")
            //"gunzip".runCommand
            // https://stackoverflow.com/questions/35421699/how-to-invoke-external-command-from-within-kotlin-code
            Runtime.getRuntime().exec("gunzip ${inFile}")
            Thread.sleep(1_000)
            inFile = File(inFile.toString().replace(".gz", ""))
            inFilea.delete()
            //println("gunziped ${inFile}a")

        }

        if ( inFile.name.endsWith("messages")) {

            //var tztemp = Unix4j.tail(1, inFile).toStringResult()
            var tztemp = Unix4j.tail(1, inFile).toStringResult()

            val ( TempDateTime ) = tztemp.split(" " )
            //println(tztemp)
            //println("temp Dte Time ${TempDateTime}")
            //println(TempDateTime)
            this.sbTimeZone = TempDateTime.substring(19,25)

            // 2018-02-26T08:43:55+00:00 user gb-edimlv-ib-gm01.ipam.rbsgrp.net python[]: info Ignoring event NetworkEvent:networks_seen#165246.1, object 28.58.1.130/31 (view: Blue), error: IBDataError: IB.Data:The network 28.58.1.128/25 must not have any active IP address outside the network you are creating.
            //var tzRegex = """"\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d(\S+)\s\S+""".toRegex()
            //var tzRegex = """".*T\d\d:\d\d:\d\d(\S+)\s.*""".toRegex()
            //var tzRegex = """"(user)""".toRegex()

            //var matchTZ = tzRegex.find(tztemp)
            //if ( matchTZ != null ) {

            //println(matchTZ!!.destructured.component1())
            //sbTimeZone = matchTZ!!.destructured.component1()
            //println("tzregex matched")
            //println(sbTimeZone)

            //} else {
            //println("tz regex not matched!")
            //}
            //this.sbTimeZone = tzRegex.find(tztemp)!!.destructured.component1()
            //println("test tail -- \n${tztemp}")
            //this.supportTransfer = Unix4j.grep("getting support file", inFile).toStringResult()

        }

        val largeOne = File(inFile.absoluteFile.toString()).readLines()

        var ( first) = largeOne[1].split(" ")
        var ( last) = largeOne[largeOne.size - 1].split(" ")
        var wet = mutableMapOf<String, Int>()
        var doraCount = mutableMapOf<String, Any>()

        var firstEpoch = OffsetDateTime.parse(first, DateTimeFormatter.ISO_DATE_TIME)
        var lastEpoch = OffsetDateTime.parse(last, DateTimeFormatter.ISO_DATE_TIME)

        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24

        var different = lastEpoch.toEpochSecond().minus(firstEpoch.toEpochSecond()).times(1000)

        val elapsedDays = different / daysInMilli
        different = different % daysInMilli

        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli

        val elapsedMinutes = different / minutesInMilli
        different = different % minutesInMilli

        val elapsedSeconds = different / secondsInMilli

        val elapsed = ("${elapsedDays} days, ${elapsedHours} hours, <br> ${elapsedMinutes} minutes, ${elapsedSeconds} seconds")

        first = first.replace("T", "<br>")
        last = last.replace("T", "<br>")

        for ( line in largeOne ) {

            val wat = "^(\\d\\d\\d\\d-\\d\\d-\\d\\d)T(\\d\\d:\\d\\d):\\d\\d(\\S+)".toRegex()
            val ( tarikh ) = line.split(" ")

            val wut = wat.find(tarikh)

            if ( wut != null ) {

                var datum = wut.destructured.component1() + " " + wut.destructured.component2()
                if ( wet.containsKey(datum)) {

                    var ttmmpp = wet[datum]
                    wet.put(datum, ttmmpp!!.plus(1))

                } else {

                    wet.put(datum, 1)

                }

            }

        }

        var sparkLineString: String? = null


        var cc = 0
        for ( ( kk, vv ) in wet ) {
            if (sparkLineString == null && cc != 0) {
                sparkLineString = kk + ", " + vv + "\n"
            } else if ( cc != wet.size.minus(1) && cc != 0 ){
                sparkLineString = sparkLineString + kk + ", " + vv + "\n"
            }
            cc = cc.plus(1)
        }

        val duh = fileAtom(inFile.name, first, last, largeOne.size.toLong(), wet , sparkLineString!!.replace("null","") , elapsed)
        files.add(duh)


        var ttt1 = Unix4j.grep("Recursion cache view", inFile).toLineList()
        var chrRegex = """(\S+)\s+\S+.+view\s+"(\S+)":\s+size\s+=\s+(\d+),\s+hits\s+=\s+(\d+),\s+misses\s+=\s+(\d+)""".toRegex()

        ttt1.forEach {

            val matchCHR = chrRegex.find(it)

            var tarikh = OffsetDateTime.parse(matchCHR?.destructured?.component1(), DateTimeFormatter.ISO_DATE_TIME)

            this.chrView.add(matchCHR?.destructured?.component2()!!.replace("_",""))
            this.chrRaw.put(  matchCHR?.destructured?.component2().replace("_",""), listOf(tarikh.toEpochSecond().toString(), matchCHR.destructured.component3(),matchCHR.destructured.component4(),matchCHR.destructured.component5()))
            this.chrRaw2.put(  matchCHR?.destructured?.component2().replace("_",""), listOf(tarikh.toEpochSecond().toString(), matchCHR!!.destructured.component1().removeRange(16,25) ,matchCHR.destructured.component3(),matchCHR.destructured.component4(),matchCHR.destructured.component5()))

            var adatum = matchCHR!!.destructured.component1().removeRange(16,25)
            var aview = matchCHR?.destructured?.component2()!!.replace("_","")
            var asize = matchCHR.destructured.component3().toLong()
            var ahits = matchCHR.destructured.component4().toLong()
            var amisses = matchCHR.destructured.component5().toLong()
            var atotal = ahits + amisses
            var aepoch = tarikh.toEpochSecond()

            val chrAtomic = chrAtom(aepoch, adatum, aview, asize, ahits, amisses, atotal)

            if ( !this.chrRaw3.containsKey(aview) ) {
                this.chrRaw3.put(aview, mutableListOf(chrAtomic))
            } else {
                this.chrRaw3[aview]?.add(chrAtomic)
            }

        }

    }

    /**
    fun fun_ptop( inFile: File ) {
        println(inFile.name)

        println("processing ${inFile.name}")

        //println("ptopfile ${inFile.toString()}")

        if ( tempNumOfCPU.not() ) {
            val cpuCount = Unix4j.grep("cpu", inFile).cut("-c", "5-9").sort().uniq().toLineList()
            //Unix4j.grep("cpu", inFile ).cut(Cut.Options.c,2).sort().uniq().toStdOut()
            //Unix4j.grep("cpu", inFile ).cut("-f", "", "1" ).sort().uniq().toStdOut()
            //Unix4j.grep("cpu", inFile ).cut(Cut.Options.c,2).toStdOut()

            this.numOfCPU = cpuCount.size - 1
            this.tempNumOfCPU = true
            //println(tempNumOfCPU.toString())
        }

        var cpuTempArray : ArrayList<String> = arrayListOf()

        var nVersionRegex = """^IDENT\s+([\S\.\-]+)\s+""".toRegex()
        var pBlankLineRegex = """^\s*$""".toRegex()

        //  MEM t 25109409792 f 2.8 b 0.4 c 35.4 s 1.3 a 13.7 sh 45.6 sw 3.8 25769799680 pio 573.3 7332.4 sio 0.1 4.8
        var pMemRegex = """^(.+)\sMEM\s+t\s+(?<total>\S+)\s+f\s+(?<memf>\S+)\s+b\s+(?<memb>\S+)\s+c\s+(?<memc>\S+)\s+s\s+(?<mems>\S+)\s+a\s+(?<mema>\S+)\s+sh\s+(?<memsh>\S+)\s+sw\s+(?<memsw1>\S+)\s+(?<memsw2>\S+)\s+pio\s+(?<mempio1>\S+)\s+(?<mempio2>\S+)\s+sio\s+(?<memsio1>\S+)\s+(?<memsio2>\S+)""".toRegex()
        var pMemRegex1 = """MEM\s+t\s+(\S+)\s+f\s+(\S+)""".toRegex()

        //  CPU cpu0 u 21.1 id/io 78.4  0.5 u/s/n  0.1  1.2 19.7 irq h/s  0.0  0.1
        //var pCPURegex = """CPU\s+(\S+)\s+u\s+(\S+)\s+id/io\s+(\S+)\s+(\S+)\s+u/s/n\s+(\S+)\s+(\S+)\s+(\S+)\s+irq h/s\s+(\S+)\s+(\S+)""".toRegex()
        var pCPURegex = """^(.+)\sCPU\s+(\S+)\s+u\s+(\S+)\s+id/io\s+(\S+)\s+(\S+)\s+u/s/n\s+(\S+)\s+(\S+)\s+(\S+)\s+irq h/s\s+(\S+)\s+(\S+)""".toRegex()

        //  TIME 13656300.4 1519204855 2018-02-21 09:20:55
        var pTimeRegex = """^TIME\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)""".toRegex()
        var  pTime :String?   =  "0000000000"
        var  pEpoch :String?  =  "0000000000"

        // SNIC qps 0 0 159312 9695 0 0 0 0 mcore 9 0 42 41 37 5 4 4 6 19 tcore 23 49 27 21 10 16 9 9 47 21 19 10 9 16 9 9 36 16 10 16 9 9 9 8 pool 98879 8556930 147836 1578693 2146983 1991701 4091 17408 cache 420372 4349 575279 242449110
        var pQPSRegex = "^SNIC qps".toRegex()

        //var ptopFile = inFile.readText()
        inFile.forEachLine {

            var matchTime = pTimeRegex.find(it)
            var matchCPU = pCPURegex.find(pTime +" "+ it)
            var matchMem = pMemRegex.find(pTime +" "+ it)
            var matchQPS = pQPSRegex.find(pTime +" "+ it)
            var matchBlank = pBlankLineRegex.find(it)
            var matchNIOSversion = nVersionRegex.find(it)

            //println(it)

            if ( matchNIOSversion != null ) {
                this.niosVersion = matchNIOSversion!!.destructured.component1()
                //println("nios version ${niosVersion}")
            }

            if (matchTime != null) {

                pTime = matchTime!!.destructured.component3() + " " + matchTime!!.destructured.component4()
                pEpoch = matchTime!!.destructured.component2()

            } else if (matchCPU != null) {

                val ( datetime , cpu, user, id, io, dunno_u,dunno_s, dunno_n, irq_h,irq_s) = matchCPU!!.destructured

                if ( cpuTempArray.size == this.numOfCPU!! + 1 ) {

                    cpuTempArray.add(user)
                    //println("cpu Temp Array count == ${cpuTempArray.size}")
                    //println("cpu Temp Array string == ${cpuTempArray.toString()}")
                    pTopCPUtemp.put( pEpoch, cpuTempArray.toList() )
                    //pTopCPU.put( pEpoch, user )
                    //println("${cpu} ${user}")
                    //println("cpuTempArray ==>> ${cpuTempArray.toList().toString()}")
                    //println("##################################")
                    cpuTempArray.clear()

                } else if ( cpuTempArray.size == 0 ) {
                    cpuTempArray.add(datetime.removeRange(16,19))
                    cpuTempArray.add(user)
                    //pTopCPU.put( pEpoch, datetime )
                    //pTopCPU.put( pEpoch, user )
                    //println("${cpu} ${user}")
                    //println("cpuTempArray ==>> ${cpuTempArray.toList().toString()}")
                } else {

                    cpuTempArray.add(user)
                    //pTopCPU.put( pEpoch, user )
                    //println("${cpu} ${user}")
                    //println("cpuTempArray ==>> ${cpuTempArray.toList().toString()}")
                }

                //pTopCPU.put(pEpoch, mutableListOf(pTime!! ) + matchCPU.groupValues.drop(1).toString())
                ////pTopCPU.put(pEpoch, matchCPU.groupValues.drop(1))
                //pTopCPU.put(pEpoch, mutableListOf(pTime!!,cpu,user,id,io,dunno_u,dunno_n,irq_h,irq_s))

                //println(matchCPU.groupValues.drop(1))
                //matchCPU.groupValues.forEach { println("cpu!!!! " + it)}
                //val kunci = pTopMmap.keySet()
                //for (key :String in kunci) {
                //println("the kunci key is >> " + key)
                //println("the key kunci is >> " + pTopMmap.get(key))
                //}
            } else if (matchMem != null) {
                //val ( mem_t, mem_f, mem_b, mem_c, mem_s, mem_a, mem_sh, mem_sw1, mem_sw2, mem_pio1, mem_pio2, mem_sio1, mem_sio2 ) = matchMem!!.destructured
                //val ( mem_t, mem_f, mem_b, mem_c, mem_s, mem_a, mem_sh, mem_sw1, mem_sw2, mem_pio1 ) = matchMem!!.destructured
                //var  mem_used :Float? = matchMem!!.destructured.component2().toFloat()
                var  mem_used :Float? = matchMem.groups["memf"]!!.value.toFloat()
                mem_used = 100 - mem_used!!
                //println(mem_used)
                var mtemp = matchMem.groupValues
                //mtemp.forEach { println("mtemp wut?? " + it) }
                //pTopMem.put(pEpoch, mutableListOf(pTime!!,"m", mem_used.toString()))
                pTopMem.put(pEpoch, matchMem.groupValues.drop(1))

            }
            /**
            else if (matchQPS != null ) {

            println(it)

            //println("snic qps")
            //println(it)
            val qpsTemp = it.split("""\s+""".toRegex())
            //var needtocleanupthis = pTime.toString().removeRange(16,25)
            var needtocleanupthis = pTime.toString().removeRange(16,19)
            //println(pTime)
            //println(pTime.toString().removeRange(16,18))


            //SNIC qps 0 0 0 0 0 0 0 0 mcore 9 0 2 1 1 1 1 1 1 1 tcore 23 1 1 1 1 1 1 0 22 1 8 1 0 1 1 1 1 22 8 8 0 1 1 1 pool 99180 8687031 151462 1583087 2173343 2009273 4091 17408 cache 30 0 47 241867510
            //println( "${qpsTemp[2]}, " + qpsTemp[2].toInt().minus(qpsTemp[3].toInt()) + ", ${qpsTemp[3]}, ${qpsTemp[4]}, " + qpsTemp[4].toInt().minus(qpsTemp[5].toInt()) + ", ${qpsTemp[5]}, ${qpsTemp[6]}, " + qpsTemp[6].toInt().minus(qpsTemp[7].toInt()) + ", ${qpsTemp[7]},  ${qpsTemp[56]}, ${qpsTemp[57]}, ${qpsTemp[58]}" )
            pQPSstring = pQPSstring + "${pTime.toString().removeRange(16,19)}, ${qpsTemp[2]}, " + qpsTemp[2].toInt().minus(qpsTemp[3].toInt()) + ", ${qpsTemp[3]}, ${qpsTemp[4]}, " + qpsTemp[4].toInt().minus(qpsTemp[5].toInt()) + ", ${qpsTemp[5]}, ${qpsTemp[6]}, " + qpsTemp[6].toInt().minus(qpsTemp[7].toInt()) +  ", ${qpsTemp[7]}, ${qpsTemp[8]}, " + qpsTemp[8].toInt().minus(qpsTemp[9].toInt()) + ", ${qpsTemp[9]},  ${qpsTemp[56]}, ${qpsTemp[57]}, ${qpsTemp[58]}\n"
            pQPSChart = true
            sbChartSignal.put("snicQPS", true)

            }
             **/

            if ( it.contains("^SNIC qps".toRegex())) {
                //println(it)
                //println("snic qps")
                //println(it)
                val qpsTemp = it.split("""\s+""".toRegex())
                //var needtocleanupthis = pTime.toString().removeRange(16,25)
                var needtocleanupthis = pTime.toString().removeRange(16,19)
                //println(pTime)
                //println(pTime.toString().removeRange(16,18))

                //println( it.toString())
                val tR = "qps (.+) mcore (.+) tcore (.+) pool (.+) cache (.+) ".toRegex()

                val tR2 = tR.find(it)

                /*
                var tMcore = tR2!!.destructured.component1().split(" ").toList()
                var tTcore = tR2!!.destructured.component2().split(" ").toList()
                var tPool = tR2!!.destructured.component3().split(" ").toList()
                var tCache = tR2!!.destructured.component4().split(" ").toList()
                println(tMcore)
                println(tTcore)
                println(tPool)
                println(tCache)
                */
                var tCpu = tR2!!.destructured.component1().replace(" ", ",")
                var tMcore = tR2!!.destructured.component2().replace(" ", ",")
                var tTcore = tR2!!.destructured.component3().replace(" ", ",")
                var tPool = tR2!!.destructured.component4().replace(" ", ",")
                //var tCache = tR2!!.destructured.component5().replace(" ", ",")
                var tCache = tR2!!.destructured.component5().split(" ").toList()
                //val tCache = ttCache.split(",").toList()

                //println(tCache.toString())


                //SNIC qps 0 0 0 0 0 0 0 0 mcore 9 0 2 1 1 1 1 1 1 1 tcore 23 1 1 1 1 1 1 0 22 1 8 1 0 1 1 1 1 22 8 8 0 1 1 1 pool 99180 8687031 151462 1583087 2173343 2009273 4091 17408 cache 30 0 47 241867510
                //println( "${qpsTemp[2]}, " + qpsTemp[2].toInt().minus(qpsTemp[3].toInt()) + ", ${qpsTemp[3]}, ${qpsTemp[4]}, " + qpsTemp[4].toInt().minus(qpsTemp[5].toInt()) + ", ${qpsTemp[5]}, ${qpsTemp[6]}, " + qpsTemp[6].toInt().minus(qpsTemp[7].toInt()) + ", ${qpsTemp[7]},  ${qpsTemp[56]}, ${qpsTemp[57]}, ${qpsTemp[58]}" )
                //pQPSstring = pQPSstring + "${pTime.toString().removeRange(16,19)}, ${qpsTemp[2]}, " + qpsTemp[2].toInt().minus(qpsTemp[3].toInt()) + ", ${qpsTemp[3]}, ${qpsTemp[4]}, " + qpsTemp[4].toInt().minus(qpsTemp[5].toInt()) + ", ${qpsTemp[5]}, ${qpsTemp[6]}, " + qpsTemp[6].toInt().minus(qpsTemp[7].toInt()) +  ", ${qpsTemp[7]}, ${qpsTemp[8]}, " + qpsTemp[8].toInt().minus(qpsTemp[9].toInt()) + ", ${qpsTemp[9]},  ${qpsTemp[56]}, ${qpsTemp[57]}, ${qpsTemp[58]}\n"
                //pQPSstring = pQPSstring + "${pTime.toString().removeRange(16,19)}, ${qpsTemp[2]}, " + qpsTemp[2].toInt().minus(qpsTemp[3].toInt()) + ", ${qpsTemp[3]}, ${qpsTemp[4]}, " + qpsTemp[4].toInt().minus(qpsTemp[5].toInt()) + ", ${qpsTemp[5]}, ${qpsTemp[6]}, " + qpsTemp[6].toInt().minus(qpsTemp[7].toInt()) +  ", ${qpsTemp[7]}, ${qpsTemp[8]}, " + qpsTemp[8].toInt().minus(qpsTemp[9].toInt()) + ", ${qpsTemp[9]}," + " ${tCache} \n"
                pQPSstring = pQPSstring + "${pTime.toString().removeRange(16,19)}, ${qpsTemp[2]}, " + qpsTemp[2].toInt().minus(qpsTemp[3].toInt()) + ", ${qpsTemp[3]}, ${qpsTemp[4]}, " + qpsTemp[4].toInt().minus(qpsTemp[5].toInt()) + ", ${qpsTemp[5]}, ${qpsTemp[6]}, " + qpsTemp[6].toInt().minus(qpsTemp[7].toInt()) +  ", ${qpsTemp[7]}, ${qpsTemp[8]}, " + qpsTemp[8].toInt().minus(qpsTemp[9].toInt()) + ", ${qpsTemp[9]}," + " ${tCache[0]}, ${tCache[2]} , ${tCache[3]} \n"
                //println(pQPSstring)
                pQPSChart = true
                sbChartSignal.put("snicQPS", true)
            }

        }   // end of inFile.forEachLine



        //println("\t\t ptop file is : $inFile")

        //println(pTopMmap.get("1519297409"))
        //for ( dum in pTopMmap.toString()) {
        //for ( dum in pTopMmap.keys()) {
        //println("DumDum in Bundle is -->> $dum")
        //}


        /**
        if ( tempNumOfCPU.not() ) {
        val cpuCount = Unix4j.grep("cpu", inFile).cut("-c", "5-9").sort().uniq().toLineList()
        //Unix4j.grep("cpu", inFile ).cut(Cut.Options.c,2).sort().uniq().toStdOut()
        //Unix4j.grep("cpu", inFile ).cut("-f", "", "1" ).sort().uniq().toStdOut()
        //Unix4j.grep("cpu", inFile ).cut(Cut.Options.c,2).toStdOut()

        this.numOfCPU = cpuCount.size - 1
        this.tempNumOfCPU = true
        }
         **/
    }

    **/

    fun fun_networkIF ( inFile: File) {

        println("processing network stuff")

        /*
        inet4   --  25
        inet6   --  27
        eth1      Link encap:Ethernet  HWaddr 2C:44:FD:7B:E5:32
                    inet addr:11.0.65.11  Bcast:11.0.65.255  Mask:255.255.255.0
                    inet6 addr: fe80::2e44:fdff:fe7b:e532/64 Scope:Link
                    UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
                    RX packets:122107621 errors:0 dropped:0 overruns:0 frame:0
                    TX packets:14178681 errors:0 dropped:0 overruns:0 carrier:0
                    collisions:0 txqueuelen:10000
                    RX bytes:11762025180 (10.9 GiB)  TX bytes:11871118675 (11.0 GiB)
                    Interrupt:32

        eth2:1      Link encap:Ethernet  HWaddr 00:00:5E:00:01:0A
                    inet addr:11.0.65.12  Bcast:11.0.65.255  Mask:255.255.255.0
                    UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
                    Interrupt:36
        */


        /* NOT WORKING!!!!!! NEED TO RECODE!!!!!!!!! */
        this.ifconfig = inFile.readText().trim()
        val ifconfigFile = inFile.readText().trim()
        //val eth1Regex = Regex("""Link encap:(\w+)\s+HWaddr\s+(\S+)\s+inet\s+addr:(\S+)\s+""", RegexOption.MULTILINE)
        //val eth1Regex = Regex("""^(\S+)\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val ethv4Regex = Regex("""^(\S+)\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val ethv6Regex = Regex("""^(\S+)\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)

        val eth04Regex = Regex("""^eth0\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val eth06Regex = Regex("""^eth0\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val eth14Regex = Regex("""^eth1\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val eth16Regex = Regex("""^eth1\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val eth24Regex = Regex("""^eth2\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val eth26Regex = Regex("""^eth2\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val eth2a4Regex = Regex("""^eth2:1\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val eth34Regex = Regex("""^eth3\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val eth36Regex = Regex("""^eth3\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val TunRegex = Regex("""^tun1\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+P-t-P:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)""".trim(),RegexOption.MULTILINE)

        val oct04Regex = Regex("""^oct0\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val oct06Regex = Regex("""^oct0\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val oct14Regex = Regex("""^oct1\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val oct16Regex = Regex("""^oct1\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val oct24Regex = Regex("""^oct2\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val oct26Regex = Regex("""^oct2\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val oct2a4Regex = Regex("""^oct2:1\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val oct34Regex = Regex("""^oct3\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)
        val oct36Regex = Regex("""^oct3\s+Link encap:(\w+)\s+HWaddr\s+(\S+)[\r|\n|\s]\s+inet addr:(\S+)\s+Bcast:(\S+)\s+Mask:(\S+)[\r|\n|\s]\s+inet6\s+addr:\s+(\S+)\s+Scope:(\S+)[\r|\n|\s]\s+(.+)MTU:(\S+)\s+Metric:(\S+)[\r|\n|\s]\s+RX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+frame:(\d+)[\r|\n|\s]\s+TX\s+packets:(\d+)\s+errors:(\d+)\s+dropped:(\d+)\s+overruns:(\d+)\s+carrier:(\d+)[\r|\n|\s]\s+collisions:(\d+)\s+txqueuelen:(\d+)[\r|\n|\s]\s+RX\s+bytes:(\d+)\s+\((.+)\)\s+TX bytes:(\d+)\s+\((.+)\)[\r|\n|\s]\s+Interrupt:(\d+)""".trim(),RegexOption.MULTILINE)

        val matchEth04 = eth04Regex.find(ifconfigFile)
        val matchEth06 = eth06Regex.find(ifconfigFile)
        val matchEth14 = eth14Regex.find(ifconfigFile)
        val matchEth16 = eth16Regex.find(ifconfigFile)
        val matchEth24 = eth24Regex.find(ifconfigFile)
        val matchEth26 = eth26Regex.find(ifconfigFile)
        val matchEth2a4 = eth2a4Regex.find(ifconfigFile)
        val matchEth34 = eth34Regex.find(ifconfigFile)
        val matchEth36 = eth36Regex.find(ifconfigFile)

        val matchOct04 = oct04Regex.find(ifconfigFile)
        val matchOct06 = oct06Regex.find(ifconfigFile)
        val matchOct14 = oct14Regex.find(ifconfigFile)
        val matchOct16 = oct16Regex.find(ifconfigFile)
        val matchOct24 = oct24Regex.find(ifconfigFile)
        val matchOct26 = oct26Regex.find(ifconfigFile)
        val matchOct2a4 = oct2a4Regex.find(ifconfigFile)
        val matchOct34 = oct34Regex.find(ifconfigFile)
        val matchOct36 = oct36Regex.find(ifconfigFile)

        val matchTun = TunRegex.find(ifconfigFile)

        if ( matchOct04 != null ) {
            this.netMGMT= matchOct04.destructured.toList()
        }
        if ( matchOct06 != null ) {
            this.netMGMT= matchOct06.destructured.toList()
        }
        if ( matchEth04 != null ) {
            this.netMGMT= matchEth04.destructured.toList()
        }
        if ( matchEth06 != null ) {
            this.netMGMT= matchEth06.destructured.toList()
        }
        if ( matchOct14 != null ) {
            this.netLAN1= matchOct14.destructured.toList()
        }
        if ( matchOct16 != null ) {
            this.netLAN1= matchOct16.destructured.toList()
        }
        if ( matchEth14 != null ) {
            this.netLAN1= matchEth14.destructured.toList()
        }
        if ( matchEth16 != null ) {
            this.netLAN1= matchEth16.destructured.toList()
        }
        if ( matchOct24 != null ) {
            this.netHA= matchOct24!!.destructured.toList()
        }
        if ( matchOct26 != null ) {
            this.netHA= matchOct26!!.destructured.toList()
        }
        if ( matchOct2a4 != null ) {
            this.netVIP= matchOct2a4!!.destructured.toList()
        }
        if ( matchEth24 != null ) {
            this.netHA= matchEth24!!.destructured.toList()
        }
        if ( matchEth26 != null ) {
            this.netHA= matchEth26!!.destructured.toList()
        }
        if ( matchEth2a4 != null ) {
            this.netVIP= matchEth2a4!!.destructured.toList()
        }
        if ( matchOct34 != null ) {
            this.netLAN2= matchOct34!!.destructured.toList()
        }
        if ( matchOct36 != null ) {
            this.netLAN2= matchOct36!!.destructured.toList()
        }
        if ( matchEth34 != null ) {
            this.netLAN2= matchEth34!!.destructured.toList()
        }
        if ( matchEth36 != null ) {
            this.netLAN2= matchEth36!!.destructured.toList()
        }
        if ( matchTun != null ) {
            this.netTun= matchTun!!.destructured.toList()
        }
    }

    fun fun_services( inFile: File) {

        println("processing some stuff")

        this.niosServices.put(inFile.name.toString(), inFile.readText().trim() )

    }

}



data class chrAtom ( var epoch: Long, var datetime: String, var view: String, var size: Long, var hits: Long, var misses: Long , var total: Long)
data class fileAtom (var name: String, var startDatum: String, var endDatum: String, var numOfLines: Long, var spark: MutableMap<String, Int> , var sparkString: String , var elapsed: String)