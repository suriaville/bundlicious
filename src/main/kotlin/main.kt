import io.javalin.Javalin
import io.javalin.rendering.template.TemplateUtil
import org.apache.commons.cli.*
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

fun main(args: Array<String>) {

    val directory = Option("d", "directory", true, "support bundle directory")
    directory.isRequired = true

    val output = Option("o", "output", true, "placeholder 4 future use")
    directory.isRequired = true

    val iFile = Option("f", "file", true, "placeholder 4 future use")
    iFile.isRequired = true

    val primaryGroup = OptionGroup().addOption(directory).addOption(iFile)
    primaryGroup.isRequired = true

    val secondaryGroup = OptionGroup()

    val options = Options().addOptionGroup(primaryGroup).addOptionGroup(secondaryGroup).addOption(output)

    val parser = DefaultParser()
    val formatter = HelpFormatter()
    var cmd: CommandLine? = null

    try {

        cmd = parser.parse(options, args)

    } catch (e: ParseException) {

        println(e.message)
        formatter.printHelp("bundlicious -d extracted_supportbundle_directory", options)
        System.exit(1)
    }

    val sbDirectory = cmd?.getOptionValue("d")
    val sbFile = cmd?.getOptionValue("file")
    val sbOutput = cmd?.getOptionValue("output")


    if (!sbFile.isNullOrBlank() || !sbOutput.isNullOrBlank()) {
        formatter.printHelp("bundlicious -d extracted_supportbundle_directory", options)
        System.exit(1)
    }

    if (!sbDirectory.isNullOrBlank()
            && Files.isDirectory(Paths.get(sbDirectory))
            && Files.isReadable(Paths.get(sbDirectory))
    //&& Files.isWritable(Paths.get(sbDirectory))
    ) {


    } else {
        System.err.println("input directory" + Paths.get(sbDirectory) + " does not exists or unreablable!")
        System.exit(1)
    }

    val sbundle = Bundle(sbDirectory)

    if (sbundle.hwtype != null) {

        sbundle.files.sortBy { it.name }
        sbundle.files.sortByDescending { it.name }
        println("############################################################################################################")

        webApp(sbundle)
    } else {
        println("Bye!")
        System.exit(404)
    }


}


fun webApp(sb: Bundle) {

    var portNumber: Int? = null

    if (System.getProperty("os.name").contains("Windows")) {

        portNumber = 6969

    } else {

        val tempIDNumber = Runtime.getRuntime().exec("id -u")
        Scanner(tempIDNumber.inputStream).use {
            portNumber = it.nextLine().toInt().plus(50000)
        }
    }

    System.setProperty("org.eclipse.jetty.util.log.class", "org.eclipse.jetty.util.log.StdErrLog")
    System.setProperty("org.eclipse.jetty.LEVEL", "OFF")

    val app = Javalin.create().disableStartupBanner().enableCaseSensitiveUrls().enableStaticFiles("/umum").start(portNumber!!.toInt())

    app.get("/demo") { ctx -> ctx.result("Hello, World!") }
    app.get("/hostname") { ctx -> ctx.result("Hello! ${sb.ifconfig}") }
    app.get("/w3") { ctx -> ctx.render("/templates/w3.ftl", TemplateUtil.model("sb", sb)) }
    app.get("/") { ctx -> ctx.render("/templates/index.ftl", TemplateUtil.model("sb", sb)) }
    app.get("/ptopCPU.csv") { ctx -> ctx.result(sb.pTopCPUString!!) }
    app.get("/ptopMem.csv") { ctx -> ctx.result(sb.pTopMemString!!) }
    app.get("/ptopCPUMEM.csv") { ctx -> ctx.result(sb.pTopCPUMemString!!) }
    app.get("/pQPS.csv") { ctx -> ctx.result(sb.pQPSstring!!) }
    app.get("/charts") { ctx -> ctx.render("/templates/charts.ftl", TemplateUtil.model("sb", sb)) }

    for (k in sb.files) {
        app.get("/spark_${k.name.replace(".", "")}.csv") { ctx -> ctx.result(k.sparkString) }
    }
    for (y in sb.chrString.keys) {
        app.get("/chr_${y}.csv") { ctx -> ctx.result(sb.chrString[y]!!) }
    }
}
