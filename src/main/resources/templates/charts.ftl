<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/w3.css">
    <link rel="stylesheet" type="text/css" href="kns.css">
    <script type="text/javascript"
        src="dygraph.js"></script>
    <script type="text/javascript"
        src="synchronizer.js"></script>
    <link rel="stylesheet" src="dygraph.css" />
    <style> body{
                background-image: url("wallpaperv5b.jpg");
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: bottom right;
            }
            .w3-theme-ibYellow {color:#000 !important; background-color:#F0EA56 !important}
            .w3-theme-ibFirstGreen {color:#000 !important; background-color:#95C85E !important}
            .w3-theme-ibSecondGreen {color:#000 !important; background-color:#6FB83B !important}
            .w3-theme-ibTeal {color:#000 !important; background-color:#55BDB1 !important}
            .w3-theme-ibFirstBlue {color:#000 !important; background-color:#64C7E1 !important}
            .w3-theme-ibSecondBlue {color:#000 !important; background-color:#0093D6 !important}
            .w3-theme-ibThirdBlue {color:#000 !important; background-color:#0078C0 !important}
    </style>
    <script>
        function tada(xid,yid) {
            var x = document.getElementById(xid);
            var y = document.getElementById(yid);
            if (x.className.indexOf("w3-show") == -1 ) {
                x.className += " w3-show";
                y.className += " w3-show";
            } else {
                x.className = x.className.replace(" w3-show", "");
                y.className = y.className.replace(" w3-show", "");
            }
        }
    </script>

    <title>bundlicious charts</title>
  </head>
  <body>

        <div class="w3-container" style="max-width:2000px;margin-top:15px"> <!-- first div -->

    <a href="/"><div class="fab">T</div></a>

            <div class="w3-card-4" style="width:1300px; margin: 0 auto;">
                    <header class="w3-container w3-blue" onclick="tada('cpumemDiv','cpumemFooterDiv')">
                    <h5>&lt;cpu & mem&gt;</h5>
                    </header>
                <div class="w3-container w3-row w3-hide w3-show" id='cpumemDiv'>

                    <table style="width:100%">
                    <tr>
                    <td style="width:80%">
                        <div id="cpumem" class="w3-col" style="width: 100%; margin: 20px;"></div>
                    </td>
                    <td>
                        <div id="cpumemlabels" style="w3-rest"></div>
                    </td>
                    </tr>
                    <tr>
                    <td>
                            <b>Display: </b><br>
                                    <input type=checkbox id="0" onClick="pCPUchange(this)" checked>
                                    <label for="0">cpu</label>
                            <#list 0 .. sb.numOfCPU -1 as y>
                                    <input type=checkbox id="${y + 1}" onClick="pCPUchange(this)" checked>
                                    <label for="${y + 1}">cpu${y}</label>
                            </#list>
                                    <input type=checkbox id="${sb.numOfCPU + 1}" onClick="pCPUchange(this)" checked>
                                    <label for="${sb.numOfCPU + 1}">Memory</label>
                    </td>
                    </tr>
                    <tr><td>
                    <a href="ptopCPUMEM.csv" target="_blank">raw data</a>
                    </td></tr>
                    </table>

                    <script type="text/javascript">

                    </script>

                </div>
                    <footer class="w3-container w3-blue w3-hide w3-show" id='cpumemFooterDiv' onclick="tada('cpumemDiv','cpumemFooterDiv')">
                    <h5>&lt;/cpu & mem&gt;</h5>
                </footer>
            </div>

            <br>

    <#if sb.sbChartSignal.snicQPS == true >

            <div class="w3-card-4" style="width:1300px; margin: 0 auto;">
                                <header class="w3-container w3-blue" onclick="tada('pQPSDiv','pQPSFooterDiv')">
                                <h5>&lt;QPS&gt;</h5>
                                </header>
                            <div class="w3-container w3-row w3-hide w3-show" id='pQPSDiv'>
                                <table style="width:100%">
                                <tr>
                                <td style="width:80%">
                                    <div id="pqps" style="width:100%; margin: 20px"></div>
                                </td>
                                <td>
                                    <div id="pqpslabels"></div>
                                </td>
                                </tr>
                                <tr>
                                <td>
                                        <b>Display: </b><br>
                                        <table>
                                            <tr>
                                            <td>
                                                <input type=checkbox id="0" onClick="DchangepQPS(this)" checked>
                                                <label for="a0">oct0TotalQPS</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="1" onClick="DchangepQPS(this)" checked>
                                                <label for="a1">oct0SnicQPS</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="2" onClick="DchangepQPS(this)" checked>
                                                <label for="a2">oct0QPStoBIND</label>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <input type=checkbox id="3" onClick="DchangepQPS(this)" checked>
                                                <label for="a3">oct1TotalQPS</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="4" onClick="DchangepQPS(this)" checked>
                                                <label for="a4">oct1SnicQPS</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="5" onClick="DchangepQPS(this)" checked>
                                                <label for="a5">oct1QPStoBIND</label>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <input type=checkbox id="6" onClick="DchangepQPS(this)" checked>
                                                <label for="a6">oct2TotalQPS</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="7" onClick="DchangepQPS(this)" checked>
                                                <label for="a7">oct2SnicQPS</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="8" onClick="DchangepQPS(this)" checked>
                                                <label for="a8">oct2QPStoBIND</label>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <input type=checkbox id="9" onClick="DchangepQPS(this)" checked>
                                                <label for="a9">oct3TotalQPS</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="10" onClick="DchangepQPS(this)" checked>
                                                <label for="a10">oct3SnicQPS</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="11" onClick="DchangepQPS(this)" checked>
                                                <label for="a11">oct3QPPStoBIND</label>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <input type=checkbox id="12" onClick="DchangepQPS(this)" checked>
                                                <label for="a12">ActiveCacheInSnic</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="13" onClick="DchangepQPS(this)" checked>
                                                <label for="a13">PrefetchToBIND</label>
                                            </td>
                                            <td>
                                                <input type=checkbox id="14" onClick="DchangepQPS(this)" checked>
                                                <label for="a14">FreeInSnic</label>
                                            </td>
                                            <td>
                                            </tr>
                                            <tr><td>
                                            <a href="pQPS.csv" target="_blank">raw data</a>
                                            </td></tr>
                                        </table>
                                </td>
                                </tr>

                                </table>

                            </div>
                                <footer class="w3-container w3-blue w3-hide w3-show" id='pQPSFooterDiv' onclick="tada('pQPSDiv','pQPSFooterDiv')">
                                <h5>&lt;/QPS&gt;</h5>
                            </footer>
                        </div>

            <br>

</#if>
<#if sb.chrNum == true >

    <#list sb.chrString as kunci, nilai >

            <div class="w3-card-4" style="width:1300px; margin: 0 auto;">
                    <header class="w3-container w3-blue" onclick="tada('${kunci}Div','${kunci}_FooterDiv')">
                    <h5>&lt;chr ${kunci} view&gt;</h5>
                    </header>

                        <div class="w3-container w3-row w3-hide w3-show" id='${kunci}Div'>

                            <table style="width:100%">
                                <tr>
                                <td style="width:80%">
                                    <div id='chr${kunci}' class='w3-col' style='width: 100%; margin: 20px;'></div>
                                </td>
                                <td>
                                    <div id='chr${kunci}labels' style='w3-rest'></div>
                                </td>
                                </tr>
                                <tr>
                                <td>
                                        <b>Display: </b>
                                        <input type=checkbox id="0" onClick="Dchange${kunci}(this)" >
                                        <label for="b0">CacheSize</label>
                                        <input type=checkbox id="1" onClick="Dchange${kunci}(this)" checked>
                                        <label for="b1">Hits</label>
                                        <input type=checkbox id="2" onClick="Dchange${kunci}(this)" checked>
                                        <label for="b2">Missed</label>
                                        <input type=checkbox id="3" onClick="Dchange${kunci}(this)" checked>
                                        <label for="b3">Total</label>
                                        <input type=checkbox id="4" onClick="Dchange${kunci}(this)" checked>
                                        <label for="b4">QPS</label>
                                </td>
                                </tr>
                                <tr><td>
                                        <a href="chr_${kunci}.csv" target="_blank">raw data</a>
                                </td></tr>
                            </table>
                               <script type='text/javascript'>

                               </script>

                </div>
                    <footer class="w3-container w3-blue w3-hide w3-show" id='${kunci}_FooterDiv' onclick="tada('${kunci}Div','${kunci}_FooterDiv')">
                    <h5>&lt;/chr ${kunci} view&gt;</h5>
                </footer>
            </div>

            <br>

            <script text="text/javascript">

            <#list sb.chrString as kunci, nilai >
                Dc${kunci} = new Dygraph(
                    document.getElementById('chr${kunci}'),
                    'chr_${kunci}.csv',
                    {
                        axes: {
                        y: { valueRange: [-3, null] }
                    },
                    labelsKMB: true,
                    labels: [ "DateTime", "Size", "Hits", "Misses", "Total", "QPS" ],
                    visibility: [false, true, true, true, true],
                    hideOverlayOnMouseOut: false,
                    labelsDiv: document.getElementById('chr${kunci}labels'),
                    labelsSeparateLines: true,
                        highlightSeriesOpts: {
                        strokeWidth: 2,
                        strokeBorderWidth: 1,
                        highlightCircleSize: 5
                    },
                    }
                );
            </#list>

            <#list sb.chrString as kunci, nilai >
                function Dchange${kunci}(el) {
                    Dc${kunci}.setVisibility(parseInt(el.id), el.checked);
                };
            </#list>



            </script>

    </#list>
</#if>
        </div> <!-- first div end -->


<script type="text/javascript">

<#if sb.sbChartSignal.snicQPS == true >
            function DchangepQPS(el) {
                pQPS.setVisibility(el.id, el.checked);
            };
</#if>

            function pCPUchange(el) {
                DcpumemD.setVisibility(parseInt(el.id), el.checked);
            };


<#if sb.sbChartSignal.snicQPS == true >
           pQPS = new Dygraph(
                document.getElementById('pqps'),
                "pQPS.csv",
                {
                    axes: {
                    y: { valueRange: [0, null] }
                },
                labelsKMB: true,
                labelsDiv: document.getElementById('pqpslabels'),
                labelsSeparateLines: true,
                hideOverlayOnMouseOut: false,
                highlightSeriesOpts: {
                    strokeWidth: 2,
                    strokeBorderWidth: 1,
                    highlightCircleSize: 5
                },
                }
           );
</#if>

           DcpumemD = new Dygraph(
                document.getElementById('cpumem'),
                "ptopCPUMEM.csv",
                {
                    axes: {
                    y: { valueRange: [0, 101] }
                },
                labelsDiv: document.getElementById('cpumemlabels'),
                labelsSeparateLines: true,
                showRangeSelector: true,
                hideOverlayOnMouseOut: false,
                highlightSeriesOpts: {
                    strokeWidth: 2,
                    strokeBorderWidth: 1,
                    highlightCircleSize: 5
                },
                }
          );

	        sync = Dygraph.synchronize([
	        DcpumemD
<#if sb.chrNum == true >
    <#list sb.chrString as kunci, nilai >
            ,Dc${kunci}
    </#list>
</#if>
<#if sb.sbChartSignal.snicQPS == true >
	        ,pQPS
</#if>
	        ], {
		    zoom: true,
		    range: false,
		    errorBars: true,
		    selection: true
	    });

	    </script>

  </body>
  </html>