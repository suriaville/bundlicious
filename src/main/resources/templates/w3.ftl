<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- W3 CSS -->
    <link rel="stylesheet" href="/w3.css">

    <title>Hello, world!</title>
  </head>
  <body>
  <div class="w3-container w3-red">
    <h1>Hello, world!</h1>
  </div>
  <div class="w3-red">
    <h2> ${sb.hostname} </h2>
  </div>
  <div class="w3-red">
    <h3> ${sb.hwtype} </h3>
  </div>
  <div class="w3-red">
    <h4> ${sb.cluster} </h4>
  </div>
  <div class="w3-red">
    <h5> ${sb.sn} </h5>
  </div>
  <div class="w3-red">
    <h6> ${sb.numOfCPU} </h6>
  </div>
  <div class="w3-red">
    <pre>${sb.upgrade} </pre>
  </div>
  <div class="w3-red">
    <pre>${sb.ifconfig} </pre>
  </div>

  </body>
</html>

