<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/w3.css">
    <link rel="stylesheet" href="/kns.css">
        <script type="text/javascript"
            src="dygraph.js"></script>
        <script type="text/javascript"
            src="synchronizer.js"></script>
        <link rel="stylesheet" src="dygraph.css" />
    <style> body{
                background-image: url("wallpaperv5b.jpg");
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: bottom right;
            }
            .w3-theme-ibYellow {color:#000 !important; background-color:#F0EA56 !important}
            .w3-theme-ibFirstGreen {color:#000 !important; background-color:#95C85E !important}
            .w3-theme-ibSecondGreen {color:#000 !important; background-color:#6FB83B !important}
            .w3-theme-ibTeal {color:#000 !important; background-color:#55BDB1 !important}
            .w3-theme-ibFirstBlue {color:#000 !important; background-color:#64C7E1 !important}
            .w3-theme-ibSecondBlue {color:#000 !important; background-color:#0093D6 !important}
            .w3-theme-ibThirdBlue {color:#000 !important; background-color:#0078C0 !important}
    </style>
    <script>
        function tada(id) {
            var x = document.getElementById(id);
            if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
            } else {
                x.className = x.className.replace(" w3-show", "");
            }
        }
    </script>
    <title>bundlicious v2</title>
  </head>
  <body>

<div class="w3-container" style="max-width:2000px;margin-top:15px">

    <a href="/charts"><div class="fab">C</div></a>

  <div class="w3-card w3-hover-shadow w3-mobile w3-left-align" style="width:1300px; margin: 0 auto;">
  <header class="w3-btn w3-block w3-left-align w3-container w3-theme-ibYellow" onclick="tada('gridDiv')">
  <!--
     <button onclick="tada('gridDiv')" class="w3-btn w3-block w3-left-align">
     </button>
  -->
     <h4>&lt;grid&gt;</h4>
  </header>
  <div class="w3-container w3-hover-shadow w3-hide w3-show" id="gridDiv">
    <table class="w3-table w3-hoverable">
        <tr><td><h3>hostname</h3></td><td><h1>${sb.hostname}</h1>support bundle is captured @ ${sb.supportTransfer[0]} ( ${sb.sbTimeZone} ) with ${sb.supportTransfer[1]} option selected </td></tr>
        <tr><td><h3>hardware type</h3></td><td><h1>${sb.hwtype}</h1></td></tr>
        <tr><td><h3>cluster</h3></td><td><h1>${sb.cluster}</h1></td></tr>
        <tr><td><h3>nios version</h3></td><td><h1>${sb.niosVersion}</h1></td></tr>
    <#if sb.upgrade?? >
        <tr><td>upgrade history</td><td><pre class="w3-code notranslate w3-tiny">${sb.upgrade}</pre></td></tr>
    </#if>
        <tr><td>serial number</td><td>${sb.sn}</td></tr>
        <tr><td>hardware id</td><td>${sb.hwid}</td></tr>
        <tr><td>licenses</td><td><pre class="w3-code w3-small">${sb.licenses}</pre></td></tr>
    </table>
  </div>
  <footer class="w3-container w3-theme-ibYellow w3-hover-shadow w3-btn w3-block w3-left-align" onclick="tada('gridDiv')"  >
     <!-- <button onclick="tada('gridDiv')" class="w3-button w3-block w3-left-align">
     </button>
     -->
     <h4>&lt;/grid&gt;</h4>
  </footer>
  </div>

<br> <!-- light-green -->

  <div class="w3-card w3-hover-shadow w3-mobile " style="width:1300px; margin: 0 auto;">
  <header class="w3-container w3-theme-ibFirstGreen w3-hover-shadow w3-btn w3-block w3-left-align" onclick="tada('snsDiv')">
    <h4>&lt;system & services&gt;</h4>
  </header>

  <div align="center" class="w3-container w3-hover-shadow w3-cell-row  w3-hide w3-show" id='snsDiv'>

    <div class="w3-row" style="margin:10px 10px 10px 10px">

    <#if sb.niosServices["DNS"]?? && sb.niosServices["DNS"] == "enabled">
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-green w3-center">
            <header style="1px 1px 1px 1px">DNS</header>
        </div>
    <#else>
        <div class="w3-col l3 m3" >
            <div class="w3-card w3-xlarge w3-red w3-center" style="margin:2px 0 2px 0">
                <header style="margin:1px 1px 1px 0px">DNS</header>
            </div>
        </div>
    </#if>

    <#if sb.niosServices["DCHP"]?? && sb.niosServices["DCHP"] == "enabled">
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-green w3-center" style="margin:2px 0 2px 0">
            <header>DHCP</header>
        </div>
    <#else>
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-red w3-center" style="margin:2px 0 2px 0">
            <header>DHCP</header>
        </div>
    </#if>

    <#if sb.niosServices["NTP"]?? && sb.niosServices["NTP"] == "enabled">
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-green w3-center" style="margin:2px 0 2px 0">
            NTP
        </div>
    <#else>
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-red w3-center" style="margin:2px 0 2px 0">
            NTP
        </div>
    </#if>

    <#if sb.niosServices["SSH"]?? && sb.niosServices["SSH"] == "enabled" >
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-green w3-center" style="margin:2px 0 2px 0">
            SSH
        </div>
    <#else>
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-red w3-center" style="margin:2px 0 2px 0">
            SSH
        </div>
    </#if>

    </div>
    <div class="w3-row" style="margin:margin:1px 1px 1px 1px">

    <#if sb.niosServices["SNMP"]?? && sb.niosServices["SNMP"] == "enabled">
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-green w3-center" style="margin:2px 0 2px 0">
            SNMP
        </div>
    <#else>
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-red w3-center" style="margin:2px 0 2px 0">
            SNMP
        </div>
    </#if>

    <#if sb.niosServices["FTP"]?? && sb.niosServices["FTP"] == "enabled">
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-green w3-center" style="margin:2px 0 2px 0">
            FTP
        </div>
    <#else>
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-red w3-center" style="margin:2px 0 2px 0">
            FTP
        </div>
    </#if>

    <#if sb.niosServices["CAPTIVE_PORTAL"]?? && sb.niosServices["CAPTIVE_PORTAL"] == "enabled">
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-green w3-center" style="margin:2px 0 2px 0">
            CAPTIVE_PORTAL
        </div>
    <#else>
        <div class="w3-col l3 m3 w3-card w3-xlarge w3-red w3-center" style="margin:2px 0 2px 0">
            <header>CAPTIVE_PORTAL</header>
        </div>
    </#if>

    </div>

    <br>

    <div align="center">
    <table style="width:90% table-layout:fixed" class="w3-table w3-striped w3-bordered w3-hoverable w3-card-4 w3-big">
    <thead>
    <tr class="w3-grey">
    <th> </th>
    <th>starts</th>
    <th>ends</th>
    <th>duration</th>
    <th># of lines</th>
    <th></th>
    <th></th>
    </tr>
    </thead>
<#list sb.files as file>
    <tr>
    <td> ${file.name} </td>
    <td> ${file.startDatum} </td>
    <td> ${file.endDatum} </td>
    <td> ${file.elapsed} </td>
    <td> ${file.numOfLines} </td>
    <td> <div id="spark_${file.name?replace(".","")}" style="width:150px; height:50px;"> </div>    </td>
    <td> <div id="spark_${file.name?replace(".","")}_labels" style="width:150px;" >  </td>
    </tr>
</#list>
    </table>
    </div>

    <script type="text/javascript">
<#list sb.files as file>
      var g_${file.name?replace(".","")} = new Dygraph(
            document.getElementById("spark_${file.name?replace(".","")}"),
            'spark_${file.name?replace(".","")}.csv',
            {
              axes : {
                x : {
                  drawGrid: false,
                  drawAxis : false,
                },
                y : {
                  drawGrid: false,
                  drawAxis : false,
                }
              },
              labels: [ "DateTime", "count" ],
              hideOverlayOnMouseOut: false,
              strokeWidth: 1.0,
              highlightCircleSize: 2,
              labelsDiv: document.getElementById('spark_${file.name?replace(".","")}_labels'),
              labelsSeparateLines: true
            }
          );
</#list>
    </script>

    <br/>

  </div>

  <!-- <footer class="w3-container w3-theme-ibFirstGreen w3-hover-shadow"> -->
  <footer class="w3-container w3-theme-ibFirstGreen w3-hover-shadow w3-btn w3-block w3-left-align" onclick="tada('snsDiv')" >
    <h4>&lt;/system & services&gt;</h4>
  </footer>
  </div>

<br> <!-- network teal -->

    <div class="w3-card w3-hover-shadow w3-mobile " style="width:1300px; margin: 0 auto;">       <!-- network card start -->
    <header class="w3-container w3-theme-ibSecondGreen w3-btn w3-block w3-left-align" onclick="tada('networkDiv')">                               <!-- network c header start -->
        <h4>&lt;network&gt;</h4>
    </header>                                                           <!-- network c header end -->

<div class="w3-container w3-hide w3-show" id='networkDiv'>                                             <!-- network c container start -->

<div class="w3-cell-row">
<!-- LAN1 card start -->
    <div class="w3-card-4 w3-container w3-mobile w3-cell">                                      <!-- LAN1 card start -->

        <header class="w3-container
<#if sb.netLAN1?size == 0 >
        w3-red">
<#else>
        w3-green">
</#if>
            <h1>
                LAN1
            </h1>
        </header>

        <div class="w3-container">                                                      <!-- LAN1 containter start -->
<#if sb.netLAN1?size != 0 >
        <table>
        <tr><td> </td></tr>
        <tr><td class="w3-small">type</td>
        <td>${sb.netLAN1[0]}</td></tr>
        <tr><td class="w3-small">mac</td>
        <td>${sb.netLAN1[1]}</td></tr>
        <tr><td class="w3-small">ipv4</td>
        <td class="w3-xlarge">${sb.netLAN1[2]}</td></tr>
        <tr><td class="w3-small">netmask</td>
        <td>${sb.netLAN1[4]}</td></tr>
        <tr><td class="w3-small">broadcast</td>
        <td>${sb.netLAN1[3]}</td></tr>
<#if sb.netLAN1?size == 27>
        <tr><td class="w3-small">ipv6</td>
        <td class="w3-large">${sb.netLAN1[5]}</td></tr>
        <tr><td class="w3-small">scope</td>
        <td>${sb.netLAN1[6]}</td></tr>
        <tr><td class="w3-small">mtu</td>
        <td>${sb.netLAN1[8]}</td></tr>
        <tr><td class="w3-small">metric</td>
        <td>${sb.netLAN1[9]}</td></tr>
        <tr><td class="w3-small">RX packet</td><td>${sb.netLAN1[10]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netLAN1[11]}</td><td>dropped</td><td>${sb.netLAN1[12]}</td></tr> <tr><td>overruns</td><td>${sb.netLAN1[13]}</td><td>frame</td><td>${sb.netLAN1[14]}</td></tr>
            <tr><td>bytes</td><td>${sb.netLAN1[22]}</td><td>( ${sb.netLAN1[23]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">TX packet</td><td>${sb.netLAN1[15]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netLAN1[16]}</td><td>dropped</td><td>${sb.netLAN1[17]}</td></tr> <tr><td>overruns</td><td>${sb.netLAN1[18]}</td><td>frame</td><td>${sb.netLAN1[19]}</td></tr>
            <tr><td>collisions</td><td>${sb.netLAN1[20]}</td><td>txQlen</td><td>${sb.netLAN1[21]}</td></tr>
            <tr><td>bytes</td><td>${sb.netLAN1[24]}</td><td>( ${sb.netLAN1[25]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">status</td>
        <td class="w3-medium">${sb.netLAN1[7]}</td></tr>
<#elseif sb.netLAN1?size == 25>
        <tr><td class="w3-small">mtu</td>
        <td>${sb.netLAN1[6]}</td></tr>
        <tr><td class="w3-small">metric</td>
        <td>${sb.netLAN1[7]}</td></tr>
        <tr><td class="w3-small">RX packet</td><td>${sb.netLAN1[8]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netLAN1[9]}</td><td>dropped</td><td>${sb.netLAN1[10]}</td></tr> <tr><td>overruns</td><td>${sb.netLAN1[11]}</td><td>frame</td><td>${sb.netLAN1[12]}</td></tr>
            <tr><td>bytes</td><td>${sb.netLAN1[20]}</td><td>( ${sb.netLAN1[21]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">TX packet</td><td>${sb.netLAN1[13]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netLAN1[14]}</td><td>dropped</td><td>${sb.netLAN1[15]}</td></tr> <tr><td>overruns</td><td>${sb.netLAN1[16]}</td><td>frame</td><td>${sb.netLAN1[17]}</td></tr>
            <tr><td>collisions</td><td>${sb.netLAN1[18]}</td><td>txQlen</td><td>${sb.netLAN1[19]}</td></tr>
            <tr><td>bytes</td><td>${sb.netLAN1[22]}</td><td>( ${sb.netLAN1[23]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">status</td>
        <td class="w3-large">${sb.netLAN1[5]}</td></tr>

</#if>
        </table>

</#if>

        </div>                                                                          <!-- LAN1 container end -->
    </div>                                                                          <!-- LAN1 card end -->
<!-- LAN1 card end -->



<!-- card LAN2 start -->
    <div class="w3-card-4 w3-container w3-mobile w3-cell">
        <header class="w3-container
<#if sb.netLAN2?size == 0 >
        w3-red">
<#else>
        w3-green">
</#if>
            <h1>
                LAN2
            </h1>
        </header>
        <div class="w3-container">
<#if sb.netLAN2?size != 0 >
        <table>
        <tr><td> </td></tr>
        <tr><td class="w3-small">type</td>
        <td>${sb.netLAN2[0]}</td></tr>
        <tr><td class="w3-small">mac</td>
        <td>${sb.netLAN2[1]}</td></tr>
        <tr><td class="w3-small">ipv4</td>
        <td class="w3-xlarge">${sb.netLAN2[2]}</td></tr>
        <tr><td class="w3-small">netmask</td>
        <td>${sb.netLAN2[4]}</td></tr>
        <tr><td class="w3-small">broadcast</td>
        <td>${sb.netLAN2[3]}</td></tr>
<#if sb.netLAN2?size == 27>
        <tr><td class="w3-small">ipv6</td>
        <td class="w3-large">${sb.netLAN2[5]}</td></tr>
        <tr><td class="w3-small">scope</td>
        <td>${sb.netLAN2[6]}</td></tr>
        <tr><td class="w3-small">mtu</td>
        <td>${sb.netLAN2[8]}</td></tr>
        <tr><td class="w3-small">metric</td>
        <td>${sb.netLAN2[9]}</td></tr>
        <tr><td class="w3-small">RX packet</td><td>${sb.netLAN2[10]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netLAN2[11]}</td><td>dropped</td><td>${sb.netLAN2[12]}</td></tr> <tr><td>overruns</td><td>${sb.netLAN2[13]}</td><td>frame</td><td>${sb.netLAN2[14]}</td></tr>
            <tr><td>bytes</td><td>${sb.netLAN2[22]}</td><td>( ${sb.netLAN2[23]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">TX packet</td><td>${sb.netLAN2[15]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netLAN2[16]}</td><td>dropped</td><td>${sb.netLAN2[17]}</td></tr> <tr><td>overruns</td><td>${sb.netLAN2[18]}</td><td>frame</td><td>${sb.netLAN2[19]}</td></tr>
            <tr><td>collisions</td><td>${sb.netLAN2[20]}</td><td>txQlen</td><td>${sb.netLAN2[21]}</td></tr>
            <tr><td>bytes</td><td>${sb.netLAN2[24]}</td><td>( ${sb.netLAN2[25]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">status</td>
        <td class="w3-medium">${sb.netLAN2[7]}</td></tr>
<#elseif sb.netLAN2?size == 25>
        <tr><td class="w3-small">mtu</td>
        <td>${sb.netLAN2[6]}</td></tr>
        <tr><td class="w3-small">metric</td>
        <td>${sb.netLAN2[7]}</td></tr>
        <tr><td class="w3-small">RX packet</td><td>${sb.netLAN2[8]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netLAN2[9]}</td><td>dropped</td><td>${sb.netLAN2[10]}</td></tr> <tr><td>overruns</td><td>${sb.netLAN2[11]}</td><td>frame</td><td>${sb.netLAN2[12]}</td></tr>
            <tr><td>bytes</td><td>${sb.netLAN2[20]}</td><td>( ${sb.netLAN2[21]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">TX packet</td><td>${sb.netLAN2[13]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netLAN2[14]}</td><td>dropped</td><td>${sb.netLAN2[15]}</td></tr> <tr><td>overruns</td><td>${sb.netLAN2[16]}</td><td>frame</td><td>${sb.netLAN2[17]}</td></tr>
            <tr><td>collisions</td><td>${sb.netLAN2[18]}</td><td>txQlen</td><td>${sb.netLAN2[19]}</td></tr>
            <tr><td>bytes</td><td>${sb.netLAN2[22]}</td><td>( ${sb.netLAN2[23]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">status</td>
        <td class="w3-large">${sb.netLAN2[5]}</td></tr>

</#if>
        </table>

</#if>

    </div>
</div>
<!-- card LAN2 end -->

<!-- card MGMT start -->
        <div class="w3-card-4 w3-container w3-mobile w3-cell">
            <header class="w3-container
<#if sb.netMGMT?size == 0 >
            w3-red">
<#else>
            w3-green">
</#if>
            <h1>
                MGMT
            </h1>
            </header>
        <div class="w3-container">
<#if sb.netMGMT?size != 0 >
        <table>
        <tr><td> </td></tr>
        <tr><td class="w3-small">type</td>
        <td>${sb.netMGMT[0]}</td></tr>
        <tr><td class="w3-small">mac</td>
        <td>${sb.netMGMT[1]}</td></tr>
        <tr><td class="w3-small">ipv4</td>
        <td class="w3-xlarge">${sb.netMGMT[2]}</td></tr>
        <tr><td class="w3-small">netmask</td>
        <td>${sb.netMGMT[4]}</td></tr>
        <tr><td class="w3-small">broadcast</td>
        <td>${sb.netMGMT[3]}</td></tr>
<#if sb.netMGMT?size == 27>
        <tr><td class="w3-small">ipv6</td>
        <td class="w3-large">${sb.netMGMT[5]}</td></tr>
        <tr><td class="w3-small">scope</td>
        <td>${sb.netMGMT[6]}</td></tr>
        <tr><td class="w3-small">mtu</td>
        <td>${sb.netMGMT[8]}</td></tr>
        <tr><td class="w3-small">metric</td>
        <td>${sb.netMGMT[9]}</td></tr>
        <tr><td class="w3-small">RX packet</td><td>${sb.netMGMT[10]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netMGMT[11]}</td><td>dropped</td><td>${sb.netMGMT[12]}</td></tr> <tr><td>overruns</td><td>${sb.netMGMT[13]}</td><td>frame</td><td>${sb.netMGMT[14]}</td></tr>
            <tr><td>bytes</td><td>${sb.netMGMT[22]}</td><td>( ${sb.netMGMT[23]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">TX packet</td><td>${sb.netMGMT[15]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netMGMT[16]}</td><td>dropped</td><td>${sb.netMGMT[17]}</td></tr> <tr><td>overruns</td><td>${sb.netMGMT[18]}</td><td>frame</td><td>${sb.netMGMT[19]}</td></tr>
            <tr><td>collisions</td><td>${sb.netMGMT[20]}</td><td>txQlen</td><td>${sb.netMGMT[21]}</td></tr>
            <tr><td>bytes</td><td>${sb.netMGMT[24]}</td><td>( ${sb.netMGMT[25]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">status</td>
        <td class="w3-medium">${sb.netMGMT[7]}</td></tr>
<#elseif sb.netMGMT?size == 25>
        <tr><td class="w3-small">mtu</td>
        <td>${sb.netMGMT[6]}</td></tr>
        <tr><td class="w3-small">metric</td>
        <td>${sb.netMGMT[7]}</td></tr>
        <tr><td class="w3-small">RX packet</td><td>${sb.netMGMT[8]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netMGMT[9]}</td><td>dropped</td><td>${sb.netMGMT[10]}</td></tr> <tr><td>overruns</td><td>${sb.netMGMT[11]}</td><td>frame</td><td>${sb.netMGMT[12]}</td></tr>
            <tr><td>bytes</td><td>${sb.netMGMT[20]}</td><td>( ${sb.netMGMT[21]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">TX packet</td><td>${sb.netMGMT[13]}</td></tr>
            <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netMGMT[14]}</td><td>dropped</td><td>${sb.netMGMT[15]}</td></tr> <tr><td>overruns</td><td>${sb.netMGMT[16]}</td><td>frame</td><td>${sb.netMGMT[17]}</td></tr>
            <tr><td>collisions</td><td>${sb.netMGMT[18]}</td><td>txQlen</td><td>${sb.netMGMT[19]}</td></tr>
            <tr><td>bytes</td><td>${sb.netMGMT[22]}</td><td>( ${sb.netMGMT[23]} )</td></tr>
            </table> </td></tr>
        <tr><td class="w3-small">status</td>
        <td class="w3-large">${sb.netMGMT[5]}</td></tr>

</#if>
        </table>

</#if>
        </div>
    </div>
<!-- card MGMT end -->

</div> <!-- end of div w3-cell-row -->
<div class="w3-cell-row">

<!-- card HA start -->
    <div class="w3-card-4 w3-container w3-mobile w3-cell">
        <header class="w3-container
<#if sb.netHA?size == 0 >
        w3-red">
<#else>
        w3-green">
</#if>
            <h1>
                HA
            </h1>
        </header>
        <div class="w3-container">
<#if sb.netHA?size != 0 >
                    <table>
                    <tr><td> </td></tr>
                    <tr><td class="w3-small">type</td>
                    <td>${sb.netHA[0]}</td></tr>
                    <tr><td class="w3-small">mac</td>
                    <td>${sb.netHA[1]}</td></tr>
                    <tr><td class="w3-small">ipv4</td>
                    <td class="w3-xlarge">${sb.netHA[2]}</td></tr>
                    <tr><td class="w3-small">netmask</td>
                    <td>${sb.netHA[4]}</td></tr>
                    <tr><td class="w3-small">broadcast</td>
                    <td>${sb.netHA[3]}</td></tr>
<#if sb.netHA?size == 27>
                    <tr><td class="w3-small">ipv6</td>
                    <td class="w3-large">${sb.netHA[5]}</td></tr>
                    <tr><td class="w3-small">scope</td>
                    <td>${sb.netHA[6]}</td></tr>
                    <tr><td class="w3-small">mtu</td>
                    <td>${sb.netHA[8]}</td></tr>
                    <tr><td class="w3-small">metric</td>
                    <td>${sb.netHA[9]}</td></tr>
                    <tr><td class="w3-small">RX packet</td><td>${sb.netHA[10]}</td></tr>
                        <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netHA[11]}</td><td>dropped</td><td>${sb.netHA[12]}</td></tr> <tr><td>overruns</td><td>${sb.netHA[13]}</td><td>frame</td><td>${sb.netHA[14]}</td></tr>
                        <tr><td>bytes</td><td>${sb.netHA[22]}</td><td>( ${sb.netHA[23]} )</td></tr>
                        </table> </td></tr>
                    <tr><td class="w3-small">TX packet</td><td>${sb.netHA[15]}</td></tr>
                        <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netHA[16]}</td><td>dropped</td><td>${sb.netHA[17]}</td></tr> <tr><td>overruns</td><td>${sb.netHA[18]}</td><td>frame</td><td>${sb.netHA[19]}</td></tr>
                        <tr><td>collisions</td><td>${sb.netHA[20]}</td><td>txQlen</td><td>${sb.netHA[21]}</td></tr>
                        <tr><td>bytes</td><td>${sb.netHA[24]}</td><td>( ${sb.netHA[25]} )</td></tr>
                        </table> </td></tr>
                    <tr><td class="w3-small">status</td>
                    <td class="w3-medium">${sb.netHA[7]}</td></tr>
<#elseif sb.netHA?size == 25>
                    <tr><td class="w3-small">mtu</td>
                    <td>${sb.netHA[6]}</td></tr>
                    <tr><td class="w3-small">metric</td>
                    <td>${sb.netHA[7]}</td></tr>
                    <tr><td class="w3-small">RX packet</td><td>${sb.netHA[8]}</td></tr>
                        <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netHA[9]}</td><td>dropped</td><td>${sb.netHA[10]}</td></tr> <tr><td>overruns</td><td>${sb.netHA[11]}</td><td>frame</td><td>${sb.netHA[12]}</td></tr>
                        <tr><td>bytes</td><td>${sb.netHA[20]}</td><td>( ${sb.netHA[21]} )</td></tr>
                        </table> </td></tr>
                    <tr><td class="w3-small">TX packet</td><td>${sb.netHA[13]}</td></tr>
                        <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netHA[14]}</td><td>dropped</td><td>${sb.netHA[15]}</td></tr> <tr><td>overruns</td><td>${sb.netHA[16]}</td><td>frame</td><td>${sb.netHA[17]}</td></tr>
                        <tr><td>collisions</td><td>${sb.netHA[18]}</td><td>txQlen</td><td>${sb.netHA[19]}</td></tr>
                        <tr><td>bytes</td><td>${sb.netHA[22]}</td><td>( ${sb.netHA[23]} )</td></tr>
                        </table> </td></tr>
                    <tr><td class="w3-small">status</td>
                    <td class="w3-large">${sb.netHA[5]}</td></tr>

</#if>
                    </table>

</#if>
        </div>
    </div>
<!-- card HA end -->


<!-- card VIP start -->
    <div class="w3-card-4 w3-container w3-mobile w3-cell">
        <header class="w3-container
<#if sb.netVIP?size == 0 >
        w3-red">
<#else>
        w3-green">
</#if>
            <h1>
                VIP
            </h1>
        </header>
        <div class="w3-container">
<!-- VIP table start here -->
<#if sb.netVIP?size == 9 >
                    <table>
                    <tr><td> </td></tr>
                    <tr><td class="w3-small">type</td>
                    <td>${sb.netVIP[0]}</td></tr>
                    <tr><td class="w3-small">mac</td>
                    <td>${sb.netVIP[1]}</td></tr>
                    <tr><td class="w3-small">ipv4</td>
                    <td class="w3-xlarge">${sb.netVIP[2]}</td></tr>
                    <tr><td class="w3-small">netmask</td>
                    <td>${sb.netVIP[4]}</td></tr>
                    <tr><td class="w3-small">broadcast</td>
                    <td>${sb.netVIP[3]}</td></tr>
                    <tr><td class="w3-small">mtu</td>
                    <td>${sb.netVIP[6]}</td></tr>
                    <tr><td class="w3-small">metric</td>
                    <td>${sb.netVIP[7]}</td></tr>
                    <tr><td class="w3-small">status</td>
                    <td class="w3-medium">${sb.netVIP[5]}</td></tr>
                    </table>
</#if>
<!-- VIP table end here -->
        </div>
    </div>
<!-- card VIP end -->

<!-- card TUN start -->
    <div class="w3-card-4 w3-container w3-mobile w3-cell">
        <header class="w3-container
<#if sb.netTun?size == 0 >
        w3-red">
<#else>
        w3-green">
</#if>
            <h1>
                TUN1
            </h1>
        </header>
        <div class="w3-container">
<#if sb.netTun?size != 0 >
                    <table>
                    <tr><td> </td></tr>
                    <!--
                    <tr><td class="w3-small">type</td>
                    <td>${sb.netTun[0]}</td></tr>
                    <tr><td class="w3-small">mac</td>
                    <td>${sb.netTun[1]}</td></tr>
                    -->
                    <tr><td class="w3-small">ipv4</td>
                    <td class="w3-xlarge">${sb.netTun[2]}</td></tr>
                    <tr><td class="w3-small">netmask</td>
                    <td>${sb.netTun[4]}</td></tr>
                    <tr><td class="w3-small">broadcast</td>
                    <td>${sb.netTun[3]}</td></tr>
                    <tr><td class="w3-small">mtu</td>
                    <td>${sb.netTun[6]}</td></tr>
                    <tr><td class="w3-small">metric</td>
                    <td>${sb.netTun[7]}</td></tr>
                    <tr><td class="w3-small">RX packet</td><td>${sb.netTun[8]}</td></tr>
                        <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netTun[9]}</td><td>dropped</td><td>${sb.netTun[10]}</td></tr> <tr><td>overruns</td><td>${sb.netTun[11]}</td><td>frame</td><td>${sb.netTun[12]}</td></tr>
                        <tr><td>bytes</td><td>${sb.netTun[20]}</td><td>( ${sb.netTun[21]} )</td></tr>
                        </table> </td></tr>
                    <tr><td class="w3-small">TX packet</td><td>${sb.netTun[13]}</td></tr>
                        <tr><td> </td><td> <table> <tr><td>errors</td><td>${sb.netTun[14]}</td><td>dropped</td><td>${sb.netTun[15]}</td></tr> <tr><td>overruns</td><td>${sb.netTun[16]}</td><td>frame</td><td>${sb.netTun[17]}</td></tr>
                        <tr><td>collisions</td><td>${sb.netTun[18]}</td><td>txQlen</td><td>${sb.netTun[19]}</td></tr>
                        <tr><td>bytes</td><td>${sb.netTun[22]}</td><td>( ${sb.netTun[23]} )</td></tr>
                        </table> </td></tr>
                    <tr><td class="w3-small">status</td>
                    <td class="w3-medium">${sb.netTun[5]}</td></tr>
                    </table>
</#if>
        </div>
    </div>

<!-- card Tun end -->
</div> <!-- end of div w3-cell-row -->

        <div class="w3-container w3-hover-shadow">
            <table class="w3-table w3-hoverable">
<#if sb.vrid?? >
            <tr><td><h3>VRID</h3></td><td><h3>${sb.vrid}</h3></td></tr>
</#if>
            <tr><td>ip addresses</td><td><pre class="w3-small w3-code ">${sb.ip_addresses}</pre></td></tr>
        </table>
        </div>


</div>                                                                  <!-- network c container end -->

    <footer class="w3-container w3-theme-ibSecondGreen w3-hover-shadow w3-btn w3-block w3-left-align" onclick="tada('networkDiv')">               <!-- network c footer start -->
        <h4>&lt;/network&gt;</h4>
    </footer>                                                           <!-- network c footer end -->

</div>                                                                  <!-- network card end -->




<br> <!-- cyan -->

  <div class="w3-card w3-hover-shadow w3-mobile " style="width:1300px; margin: 0 auto;">
  <header class="w3-container w3-theme-ibTeal w3-btn w3-block w3-left-align" onclick="tada('hardwareDiv')">
    <h4>&lt;hardware&gt;</h4>
  </header>
  <div class="w3-container w3-hover-shadow w3-hide" id='hardwareDiv'>
    <table class="w3-table w3-hoverable">
        <tr><td><h3>VRID</h3></td><td><h3>vrid</h3></td></tr>
    </table>
  </div>
  <footer class="w3-container w3-theme-ibTeal w3-hover-shadow w3-btn w3-block w3-left-align" onclick="tada('hardwareDiv')">
    <h4>&lt;/hardware&gt;</h4>
  </footer>
  </div>

<br> <!-- blue -->

  <div class="w3-card w3-hover-shadow w3-mobile " style="width:1300px; margin: 0 auto;">
  <header class="w3-container w3-theme-ibFirstBlue w3-btn w3-block w3-left-align" onclick="tada('miscDiv')">
    <h4>&lt;misc&gt;</h4>
  </header>
  <div class="w3-container w3-hover-shadow w3-hide" style="padding:0 0 0 0" id='miscDiv'>
            <img src="dp3.png" style="width:1300px; padding:0 0 0 0; margin:0 0 0 0">
  </div>
  <footer class="w3-container w3-theme-ibFirstBlue w3-hover-shadow w3-btn w3-block w3-left-align" onclick="tada('miscDiv')">
    <h4>&lt;/misc&gt;</h4>
  </footer>
  </div>

<br> <!-- theme-ibSecondblue -->

  <div class="w3-card w3-hover-shadow w3-mobile " style="width:1300px; margin: 0 auto;">
  <header class="w3-container w3-theme-ibSecondBlue w3-btn w3-block w3-left-align" onclick="tada('secDiv')">
    <h4>&lt;security&gt;</h4>
  </header>
  <div class="w3-container w3-hover-shadow w3-hide" style="padding:0 0 0 0" id='secDiv'>
        <img src="dp2.jpg" style="width:1300px; padding:0 0 0 0; margin:0 0 0 0">
  </div>
  <footer class="w3-container w3-theme-ibSecondBlue w3-hover-shadow w3-btn w3-block w3-left-align" onclick="tada('secDiv')">
    <h4>&lt;/security&gt;</h4>
  </footer>
  </div>

  <br> <!-- theme-ibThirdBlue -->

    <div class="w3-card w3-hover-shadow w3-mobile" style="width:1300px; margin: 0 auto">
        <header class="w3-container w3-theme-ibThirdBlue w3-btn w3-block w3-left-align" onclick="tada('analDiv')">
            <h4>&lt;analytics&gt;</h4>
        </header>
    <div class="w3-container w3-hover-shadow w3-center w3-hide" id='analDiv' style="padding:0 0 0 0">
        <img src="dp1.jpg" style="width:1300px; padding:0 0 0 0; margin:0 0 0 0">
    </div>
    <footer class="w3-container w3-theme-ibThirdBlue w3-hover-shadow w3-btn w3-block w3-left-align" onclick="tada('analDiv')">
      <h4>&lt;/analytics&gt;</h4>
    </footer>
    </div>

</div> <!-- class -->

    <h6> ${sb.numOfCPU} </h6>

  </body>
</html>


